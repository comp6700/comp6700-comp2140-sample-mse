# COMP6700/COMP2140 Sample Mid-Semester Exam

This repo contains an IntelliJ project that you can use as
 part of the **sample** mid-semester exam.

 You will find three questions, each a separate Java class with
  FIXME notes indicating what you need to do.   Each question is
  worth 5 marks.  There are 5 unit tests
  provided for each question with which you may test your answer.

In the exam, there will be six questions, and you will have about 15 minutes in which
to answer each question, (since each is worth 5/30 marks and the exam is 90 mins long).

 Important notes:
 * The exam will be **entirely auto-graded**, so it is important that your code passes the tests.
 * The exam will not involve git.  Instead **you must copy your code into your
 exam in your browser**.
 * In the exam, the state of your exam is **continuously saved** (you don't need
 to press a save button).

